import { find } from 'lodash';
import calculate from './reducer';
import * as actions from './actions';

describe('Calculate reducer ->', () => {
  const BEDTIME = new Date('March 27, 2014 20:15:00');

  it('calculate init', () => {
    expect(calculate(undefined, { type: '@@INIT' })).toEqual({});
  });

  it('should return state if no action found', () => {
    const state = { hello: 'world' };
    expect(calculate(state, { type: 'NOT_AN_ACTION_TYPE' })).toEqual(state);
  });

  it('should return warning if datetimes are more than a day apart', () => {
    const start = new Date('March 26, 2014 10:00:00');
    const end = new Date('March 27, 2014 23:00:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(res.warnings.length).toBe(1);
    expect(res.total).toEqual(0);
  });

  it('should return warning if end is before start', () => {
    const start = new Date('March 27, 2014 10:00:00');
    const end = new Date('March 26, 2014 23:00:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(res.warnings.length).toBe(1);
    expect(res.total).toEqual(0);
  });

  it('should return 1 warning if start is before 6pm and end is before 4am', () => {
    const start = new Date('March 27, 2014 10:00:00');
    const end = new Date('March 27, 2014 23:00:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(res.warnings.length).toBe(1);
  });

  it('should return 1 warning if start is after 6pm and end is after 4am', () => {
    const start = new Date('March 27, 2014 18:01:00');
    const end = new Date('March 28, 2014 05:00:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(res.warnings.length).toBe(1);
  });

  it('should return 2 warnings if start is before 6pm and end is after 4am', () => {
    const start = new Date('March 27, 2014 10:01:00');
    const end = new Date('March 28, 2014 06:00:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(res.warnings.length).toBe(2);
  });

  it('should get paid $12/hour from start-time to bedtime', () => {
    const start = new Date('March 27, 2014 18:15:00');
    const end = new Date('March 28, 2014 05:00:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(find(res.payments, { id: 'beforeBedtime' }).pay).toEqual(27);
  });

  it('should get paid $8/hour from bedtime to midnight', () => {
    const start = new Date('March 27, 2014 18:15:00');
    const end = new Date('March 28, 2014 05:00:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(find(res.payments, { id: 'betweenBedtimeAndMidnight' }).pay).toEqual(30);
  });

  it('should get paid $16/hour from midnight to end of job', () => {
    const start = new Date('March 27, 2014 18:15:00');
    const end = new Date('March 28, 2014 05:00:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(find(res.payments, { id: 'afterMidnight' }).pay).toEqual(64);
  });

  it('should get paid for full hours (no fractional hours)', () => {
    const start = new Date('March 27, 2014 18:20:00');
    const end = new Date('March 28, 2014 01:45:00');
    const res = calculate({}, actions.calculate(start, end, BEDTIME));
    expect(res.total).toEqual(89);
  });
});
