/**
 * Actions
 */

export const calculate = (start, end, bedtime) => ({
  type: 'CALCULATE',
  start: !isNaN(Date.parse(start)) ? new Date(start) : null,
  end: !isNaN(Date.parse(end)) ? new Date(end) : null,
  bedtime: !isNaN(Date.parse(bedtime)) ? new Date(bedtime) : null,
});
