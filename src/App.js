// Main component
import React from 'react';
import Calculator from './Calculator';

const App = () => (
  <div>
    <Calculator />
  </div>
);

export default App;
