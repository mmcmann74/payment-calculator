import * as actions from './actions';

describe('Actions ->', () => {
  it('calculate w/ 0 args should create CALCULATE action with NULL dates', () => {
    expect(actions.calculate()).toEqual({
      type: 'CALCULATE',
      start: null,
      end: null,
      bedtime: null,
    });
  });

  it('calculate w/ string args should create CALCULATE action with NULL dates', () => {
    expect(actions.calculate('hello', 'world')).toEqual({
      type: 'CALCULATE',
      start: null,
      end: null,
      bedtime: null,
    });
  });

  it('calculate w/ JS Date args should create CALCULATE action with dates', () => {
    const today = new Date();
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);
    expect(actions.calculate(today, yesterday)).toEqual({
      type: 'CALCULATE',
      start: today,
      end: yesterday,
      bedtime: null,
    });
  });
});
