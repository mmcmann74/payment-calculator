import moment from 'moment';
import { ceilHour, floorHour, getHoursInterval } from './dateUtils';

const PRE_BEDTIME_RATE = 12.0;
const BEDTIME_TO_MIDNIGHT_RATE = 8.0;
const POST_MIDNIGHT_RATE = 16.0;

/**
 * Main reducer function to calculate payment(s) based on times entered.
 *
 * @param {Object} param0 State object with start, end, and bedtime dates
 */
function calculatePayments({ start, end, bedtime }) {
  const warnings = [];

  // Check if data is usable
  if (!start || isNaN(Date.parse(start)) || !end || isNaN(Date.parse(end))) {
    return { payments: [], total: 0, warnings };
  }

  // Set any warning messages
  if (moment(end).diff(moment(start), 'hours') > 24) {
    warnings.push('Must be the same day.');
    return { payments: [], total: 0, warnings };
  }
  if (moment(start).isAfter(moment(end))) {
    warnings.push('Dates must be sequential');
    return { payments: [], total: 0, warnings };
  }
  if (moment(start).hour() < 18) {
    warnings.push('No payment for hours before 6pm.');
  }
  if (moment(start).day() !== moment(end).day() && moment(end).hour() > 4) {
    warnings.push('No payment for hours after 4am.');
  }

  const midnight = moment(start).clone().add(1, 'days').startOf('day').toDate();
  const lbound = moment(start).clone().hour(18).minute(0).seconds(0).toDate();
  const ubound = moment(start).clone().add(1, 'days').hour(4).minute(0).seconds(0).toDate();

  const getPreBedtimeInterval = getHoursInterval(lbound, bedtime);
  const getBedtimeToMidnightInterval = getHoursInterval(bedtime, midnight);
  const getAfterMidnightInterval = getHoursInterval(midnight, ubound);

  const start2 = floorHour(start);
  const end2 = ceilHour(end);

  const h1 = getPreBedtimeInterval(start2, end2);
  const h2 = getBedtimeToMidnightInterval(start2, end2);
  const h3 = getAfterMidnightInterval(start2, end2);
  const p1 = PRE_BEDTIME_RATE * h1;
  const p2 = BEDTIME_TO_MIDNIGHT_RATE * h2;
  const p3 = POST_MIDNIGHT_RATE * h3;
  const total = p1 + p2 + p3;

  return {
    payments: [
      { id: 'beforeBedtime', hours: h1, pay: p1 },
      { id: 'betweenBedtimeAndMidnight', hours: h2, pay: p2 },
      { id: 'afterMidnight', hours: h3, pay: p3 },
    ],
    total,
    warnings,
  };
}

const calculate = (state = {}, action = { type: null }) => {
  switch (action.type) {
    case 'CALCULATE':
      return Object.assign({}, state, calculatePayments(action));
    default:
      return state;
  }
};

export default calculate;
