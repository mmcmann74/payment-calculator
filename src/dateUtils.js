// Date utility functions

const HOURS_UNITS = 3600;

/**
 * Get a UNIX-style timestamp
 */
const toTimestamp = (d) => (d.getTime() / 1000 | 0);

function lessThan(x, y) {
  return x < y;
}

function greaterThan(x, y) {
  return x > y;
}

const compare = (fn) => (bound, value) => {
  if (!(bound instanceof Date) || !(value instanceof Date)) {
    return null;
  }
  const boundTs = toTimestamp(bound);
  const valueTs = toTimestamp(value);
  if (valueTs === boundTs) {
    return value;
  }
  return fn(valueTs, boundTs) ? value : bound;
};

/**
 * Return given date or upper boundary, whichever comes first
 *
 * @param {Date} bound The upper boundary of acceptable dates
 * @param {Date} value The date being compared
 */
export const ucompare = compare(lessThan);

/**
 * Return given date or lower boundary, whichever comes first
 *
 * @param {Date} bound The lower boundary of acceptable dates
 * @param {Date} value The date being compared
 */
export const lcompare = compare(greaterThan);

const getInterval = (units) => (lower, upper) => (start, end) => {
  const start2 = (lcompare(lower, start));
  const end2 = (ucompare(upper, end));
  const diff = (toTimestamp(end2) - toTimestamp(start2)) / units;
  return diff < 0 ? 0 : diff;
};

/**
 * Calculate interval between dates in hours with boundaries.
 *
 * @param {Date} bound The upper boundary of acceptable dates
 * @param {Date} value The date being compared
 * @returns {function}
 */
export const getHoursInterval = getInterval(HOURS_UNITS);

/**
 * Curried function to either floor or ceiling the hour of a given Date.
 *
 * @param {function} fn Supply either Math.ceil or Math.floor
 */
const setHour = (fn) => (date) => {
  const newDate = new Date(date);
  newDate.setHours(
    date.getHours() +
    fn((date.getMinutes() === 60 ? 0 : date.getMinutes()) / 60)
  );
  newDate.setMinutes(0);
  newDate.setSeconds(0);
  return newDate;
};

/**
 * Get the Date with the hour rounded up.
 * @param {Date} date
 */
export const ceilHour = setHour(Math.ceil);

/**
 * Get the Date with the hour rounded down.
 * @param {Date} date
 */
export const floorHour = setHour(Math.floor);
