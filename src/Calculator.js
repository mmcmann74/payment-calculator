import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from './actions';

const CalculatorComponent = ({ start, end, bedtime, total, warnings, onCalculate }) => {
  let startout = null;
  let endout = null;
  let btout = null;

  const warningList = warnings && warnings.length > 0 ?
    warnings.map((w) => (<div className={'alert alert-warning'}>{w}</div>)) :
    null;

  return (
    <div>
      <h1>Calculate Your Payment</h1>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          onCalculate(startout.value, endout.value, btout.value);
        }}
      >
        <div className="form-group">
          <label htmlFor="startTimeInput">Start time</label>
          <input
            type="text"
            className="form-control"
            id="startTimeInput"
            placeholder="March 27, 2014 18:15:00"
            value={start}
            ref={(node) => {
              startout = node;
            }}
          />
        </div>
        <div className="form-group">
          <label htmlFor="endTimeInput">End time</label>
          <input
            type="text"
            className="form-control"
            id="endTimeInput"
            placeholder="March 28, 2014 03:15:00"
            value={end}
            ref={(node) => {
              endout = node;
            }}
          />
        </div>
        <div className="form-group">
          <label htmlFor="bedTimeInput">Bed time</label>
          <input
            type="text"
            className="form-control"
            id="bedTimeInput"
            placeholder="March 27, 2014 20:15:00"
            value={bedtime}
            ref={(node) => {
              btout = node;
            }}
          />
        </div>
        <button type="submit">Calculate</button>
        <hr />
        {warningList}
        <p>
          Payment <b>${total ? total.toFixed(2) : '0.00'}</b>
        </p>
      </form>
    </div>
  );
};

CalculatorComponent.propTypes = {
  start: PropTypes.instanceOf(Date),
  end: PropTypes.instanceOf(Date),
  bedtime: PropTypes.instanceOf(Date),
  payments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.oneOf(['beforeBedtime', 'betweenBedtimeAndMidnight', 'afterMidnight']),
    hours: PropTypes.number,
    pay: PropTypes.number,
  })),
  total: PropTypes.number,
  onCalculate: PropTypes.func,
  warnings: PropTypes.array,
};

const mapStateToProps = (state) => (state);

const mapDispatchToProps = (dispatch) => ({
  onCalculate: (start, end, bedtime) => {
    dispatch(actions.calculate(start, end, bedtime));
  },
});

const Calculator = connect(
  mapStateToProps,
  mapDispatchToProps
)(CalculatorComponent);

export default Calculator;
