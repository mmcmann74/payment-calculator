import { ceilHour, floorHour, ucompare, lcompare } from './dateUtils';

const t613 = new Date('March 26, 2014 18:13:00');
const t648 = new Date('March 26, 2014 18:48:00');
const t700 = new Date('March 26, 2014 19:00:00');
const t230 = new Date('March 27, 2014 02:30:00');

describe('Date utility functions ->', () => {
  it('should return 6:13 ceiling as 7:00', () => {
    expect(ceilHour(t613).getTime()).toEqual(new Date('March 26, 2014 19:00:00').getTime());
  });

  it('should return 6:48 ceiling as 7:00', () => {
    expect(ceilHour(t648).getTime()).toEqual(new Date('March 26, 2014 19:00:00').getTime());
  });

  it('should return 7:00 ceiling as 7:00', () => {
    expect(ceilHour(t700).getTime()).toEqual(new Date('March 26, 2014 19:00:00').getTime());
  });

  it('should return 6:13 floor as 6:00', () => {
    expect(floorHour(t613).getTime()).toEqual(new Date('March 26, 2014 18:00:00').getTime());
  });

  it('should return 6:48 floor as 6:00', () => {
    expect(floorHour(t648).getTime()).toEqual(new Date('March 26, 2014 18:00:00').getTime());
  });

  it('should return 7:00 floor as 7:00', () => {
    expect(floorHour(t700).getTime()).toEqual(new Date('March 26, 2014 19:00:00').getTime());
  });

  it('should return midnight', () => {
    expect(ucompare(new Date('March 27, 2014 00:00:00'), t230)).toEqual(new Date('March 27, 2014 00:00:00'));
  });

  it('should return 7pm', () => {
    expect(lcompare(t700, t648)).toEqual(t700);
  });
});
