module.exports = function (config) {
  config.set({
    browsers: ['PhantomJS'],
    files: [
      'tests.webpack.js',
    ],
    frameworks: [
      'jasmine',
    ],
    preprocessors: {
      'tests.webpack.js': ['webpack', 'sourcemap'],
    },
    reporters: ['progress'],
    webpack: {
      cache: true,
      // watch: true,
      devtool: 'inline-source-map',
      module: {
        rules: [
          {
            enforce: 'pre',
            test: /\.test\.js$/,
            include: /src/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['env'],
              },
            },
          },
          {
            test: /\.js$/,
            include: /src/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['env'],
              },
            },
          },
        ],
      },
    },
  });
};
