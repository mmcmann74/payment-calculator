# Babysitter Kata

## Requirements

This kata simulates a babysitter working and getting paid for one night.  The rules are pretty straight forward.

The babysitter:
- starts no earlier than 6:00PM
- leaves no later than 4:00AM
- gets paid $12/hour from start-time to bedtime
- gets paid $8/hour from bedtime to midnight
- gets paid $16/hour from midnight to end of job
- gets paid for full hours (no fractional hours)


## Feature
*As a babysitter<br>
In order to get paid for 1 night of work<br>
I want to calculate my nightly charge<br>*

## Setup

Run `npm install` (You'll need at least version 3).

## Running the app

Run `npm run start` to build the app and start the web server. Open a
browser window and navigate to http://localhost:8080/.

Enter in three dates in the form of "March 27, 2014 18:55:00".

## Building the app

Run `npm run build` to build the app into the dist/ directory.

## Tests

Run `npm run test:single`. This will launch Karma and execute all of the 
unit tests in a single run. `npm run test` will run the tests in watch
mode.

## Future enhancements

* Use a date picker for the date fields
* Display hours worked, and payment for each time block
* Store the results in a database
* Use default values for inputs based on day
